# udt4

This is a wrapper around the UDT4 library. 

See [http://en.wikipedia.org/wiki/UDP-based_Data_Transfer_Protocol](http://en.wikipedia.org/wiki/UDP-based_Data_Transfer_Protocol)

*This library is intentionally marked as 0.1.0 and should be considered alpha*

![NPM Stats](https://nodei.co/npm/udt4.png "NPM Stats")

## usage 

### server

```
var udt4 = require('../udt4');
var svr = udt4.createServer(function (data) {
      console.log("Server Callback : " + data);
})

svr.listen(1337, '127.0.0.1');

svr.on('data', function(data) {
   console.log("Explicit On : " + data)
})

svr.on('connection', function(data) {
   console.log("connection On : " + data)
})

svr.on('error', function(data) {
   console.log("Error : " + data)
})

svr.on('disconnect', function(data) {
   console.log("disconnect : " + data)
})

console.log('UDT4 Server running at 127.0.0.1:1337');
```

### client 

```
var udt4 = require('udt4');

var clt = udt4.createClient();

clt.connect(1337);

clt.write('data');

clt.end();
```

## api 

### udt4.createServer([dataListener])

Returns a new server object.

The dataListener is a function which is automatically added to the 'data' event. 

### server.listen(port)
Begin accepting connections on the specified port. As the hostname is currently omitted, the server will accept connections directed to any IPv4 address (INADDR_ANY). 

### server.close()
Stops the server from accepting new connections and keeps existing connections. This function is asynchronous, the server is finally closed when all connections are ended and the server emits a 'close' event. 

### udt4.createClient()
Constructs a new UDT4 client. port and host refer to the server to be connected to. 

### client.connect(port, [host]) 
Opens a connection to the server

### client.write(data)
Sends data to the server.

### client.end()
Closes the connection to the server.

### udt4.createReadStream(port,[address])

Create a readStream which is a wrapper around the server object.

```
var udt4 = require('udt4');

var rs = udt4.createReadStream(port,[server])

rs.pipe(process.stdout)

```



## contributors


<table><tbody>

<tr><th align="left">Anton Whalley</th><td><a href="https://gitlab.com/u/no9">GitLab/No9</a></td><td><a href="https://twitter.com/dhigit9">Twitter/@dhigit9</a></td></tr>

</tbody></table> 