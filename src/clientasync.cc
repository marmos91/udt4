#include <nan.h>
#include <uv.h>
#include <iostream>
#include <sstream>
#include <udt.h>

#include "cc.h"
#include "clientasync.h"

using namespace v8;
using namespace std;

namespace udt4 {

static Persistent<Object> persistentEmitter;
uv_loop_t *clientloop;

ClientAsync::ClientAsync(Local<Object> eventemitter, NanCallback *callback, int port)
         : NanAsyncWorker(callback), port(port) {

            NanAssignPersistent(persistentEmitter, eventemitter);
            clientloop = uv_default_loop();
            /*uv_async_init(loop, &data_async, OnDataEvent);
            uv_async_init(loop, &connection_async, OnConnectionEvent);
            uv_async_init(loop, &disconnect_async, OnDisconnectEvent);
            uv_async_init(loop, &error_async, OnErrorEvent);
              */
         }

      ClientAsync::~ClientAsync() {
        NanDisposePersistent(persistentEmitter);

      }

      void ClientAsync::Execute () {
         
         UDT::startup();
         struct addrinfo hints, *local, *peer;

         memset(&hints, 0, sizeof(struct addrinfo));

         hints.ai_flags = AI_PASSIVE;
         hints.ai_family = AF_INET;
         hints.ai_socktype = SOCK_STREAM;
         //hints.ai_socktype = SOCK_DGRAM;

         if (0 != getaddrinfo(NULL, "9000", &hints, &local))
         {
            //TODO: Raise Error
            cout << "incorrect network address.\n" << endl;
         }

         UDTSOCKET client = UDT::socket(local->ai_family, local->ai_socktype, local->ai_protocol);

         #ifdef WIN32
            UDT::setsockopt(client, 0, UDT_MSS, new int(1052), sizeof(int));
         #endif

         freeaddrinfo(local);

         std::stringstream out;
         out << port;
         string service(out.str());

         if (0 != getaddrinfo(NULL, service.c_str(), &hints, &peer))
         {
            //TODO: Raise Error
            cout << "incorrect server/peer address. " << "argv[1]" << ":" << port << endl;
            
         }

         // connect to the server, implict bind
         if (UDT::ERROR == UDT::connect(client, peer->ai_addr, peer->ai_addrlen))
         {
            //TODO: Raise Error
            cout << "connect: " << UDT::getlasterror().getErrorMessage() << endl;
            
         }

         freeaddrinfo(peer);

         // using CC method
         //CUDPBlast* cchandle = NULL;
         //int temp;
         //UDT::getsockopt(client, 0, UDT_CC, &cchandle, &temp);
         //if (NULL != cchandle)
         //   cchandle->setRate(500);

         int size = 100000;
         char* data = new char[size];

      }

      void ClientAsync::HandleOKCallback () {
         NanScope();
         //callback->Call(0, NULL);
      }
}