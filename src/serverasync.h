#include <v8.h>
#include <node.h>
#include <nan.h>
#include <uv.h>

namespace udt4 {

  class ServerAsync : public NanAsyncWorker {
    public:

	   ServerAsync(v8::Local<v8::Object> eventEmitter, NanCallback* callback, int port);
	   virtual ~ServerAsync ();
     virtual void Execute ();
     void HandleOKCallback ();

     static void Close();
     static void OnDataEvent(uv_async_t *handle, int status);
     static void OnErrorEvent(uv_async_t *handle, int status);
     static void OnConnectionEvent(uv_async_t *handle, int status);
     static void OnDisconnectEvent(uv_async_t *handle, int status);
     static void OnCloseEvent(uv_async_t *handle, int status);
	private:
		int port;
    
  }; 
}