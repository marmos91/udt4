var udt4 = require('../udt4');
var test = require('tap').test

test('test client metrics and events', function (t) {
   t.plan(8); 
   var clt = udt4.createClient();
   t.type(clt, 'object', 'Client is an object');
   console.log(clt)
   clt.on('metrics', function(data) {
      t.ok(true, 'Eventing Test');
      t.equal(data.sendrate, 1, 'Send rate is valid');
      t.equal(data.rtt, 1, 'Round Trip Time is valid');
      t.equal(data.cwnd, 1, 'Congestion Window is valid');
      t.equal(data.pktsendperiod, 1, 'Packet Send Period is valid');
      t.equal(data.recvack, 1, 'Recieved Acknowledgments is Valid');
      t.equal(data.recvnack, 1, 'Revieved Negative Acknowledgements is valid'); 

   });

   clt.firemetrics();
});
